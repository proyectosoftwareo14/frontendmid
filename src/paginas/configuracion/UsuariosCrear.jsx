import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import APIInvoke from '../../helpers/APIInvoke.js';
import { useNavigate } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes';
import Form from 'react-bootstrap/Form';

const UsuariosCrear = () => {

    const navigate = useNavigate();

    const [usuario, setUsuario] = useState({
        rol: '-8',
        ciudad: '-8',
        nombres: '',
        apellidos: '',
        tipodocumento: '-8',
        documento: '',
        celular: '',
        correo: '',
        usuario_acceso: '',
        clave_acceso: '',
        direccion: '',
        estado: 1
    });

    const { rol, ciudad, nombres, apellidos, tipodocumento, documento, celular, correo, usuario_acceso, clave_acceso, direccion } = usuario;

    const [arregloRoles, setArregloRoles] = useState([]);

    const comboRoles = async () => {
        const response = await APIInvoke.invokeGET(`/api/roles/combo-roles`);
        setArregloRoles(response);
    }

    const [arregloCiudades, setArregloCiudades] = useState([]);

    const comboCiudades = async () => {
        const response = await APIInvoke.invokeGET(`/api/ciudades/combo-ciudades`);
        setArregloCiudades(response);
    }

    useEffect(() => {
        comboRoles();
        comboCiudades();
        document.getElementById("nombres").focus();
    }, []);

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();

        if (usuario.rol === "-8") {
            mensajeConfirmacion('error', 'Debes seleccionar un rol.');
        } else if (usuario.ciudad === "-8") {
            mensajeConfirmacion('error', 'Debes seleccionar una ciudad.');
        } else if (usuario.tipodocumento === "-8") {
            mensajeConfirmacion('error', 'Debes seleccionar un tipo documento.');
        } else {
            crear();
        }
    }

    const crear = async () => {
        const body = {
            idRol: usuario.rol,
            idCiudad: usuario.ciudad,
            nombresUsuario: usuario.nombres,
            apellidosUsuario: usuario.apellidos,
            tipoDocUsuario: usuario.tipodocumento,
            documentoUsuario: usuario.documento,
            celularUsuario: usuario.celular,
            correoUsuario: usuario.correo,
            usuarioAcceso: usuario.usuario_acceso,
            claveAcceso: usuario.clave_acceso,
            estadoUsuario: usuario.estado,
            direccionUsuario: usuario.direccion
        }
        const response = await APIInvoke.invokePOST(`/api/usuarios`, body);
        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            navigate("/usuarios-admin");
        } else {
            mensajeConfirmacion('error', response.msg);
        }

        setUsuario({
            rol: '-8',
            ciudad: '-8',
            nombres: '',
            apellidos: '',
            tipodocumento: '-8',
            documento: '',
            celular: '',
            correo: '',
            usuario_acceso: '',
            clave_acceso: '',
            direccion: '',
            estado: 1
        });
    }

    return (
        <main id="main" className="main">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <ContentHeader
                Titulo={"Usuarios"}
                breadCrumb1={"Configuración"}
                breadCrumb2={"Listado Usuarios"}
                breadCrumb3={"Crear Usuarios"}
                ruta1={"/usuarios-admin"}
            />

            <section className="section">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <h5 className="card-title">Crear Usuario</h5>

                                    <div className="row mb-3">
                                        <label htmlFor="rol" className="col-sm-2 col-form-label">Seleccione un perfil</label>
                                        <div className="col-sm-10">
                                            <Form.Select aria-label="Default select example"
                                                style={{ cursor: 'pointer' }}
                                                id="rol"
                                                name="rol"
                                                value={rol}
                                                onChange={onChange}
                                            >
                                                <option value="-8">SELECCIONE</option>
                                                {
                                                    arregloRoles.map(
                                                        opcion =>
                                                            <option value={opcion._id} key={opcion._id}>{opcion.nombreRol}</option>
                                                    )
                                                }
                                            </Form.Select>
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="ciudad" className="col-sm-2 col-form-label">Seleccione una ciudad</label>
                                        <div className="col-sm-10">
                                            <Form.Select aria-label="Default select example"
                                                style={{ cursor: 'pointer' }}
                                                id="ciudad"
                                                name="ciudad"
                                                value={ciudad}
                                                onChange={onChange}
                                            >
                                                <option value="-8">SELECCIONE</option>
                                                {
                                                    arregloCiudades.map(
                                                        opcion =>
                                                            <option value={opcion._id} key={opcion._id}>{opcion.nombreCiudad}</option>
                                                    )
                                                }
                                            </Form.Select>
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="nombres" className="col-sm-2 col-form-label">Nombres</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control"
                                                id="nombres"
                                                name="nombres"
                                                value={nombres}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="apellidos" className="col-sm-2 col-form-label">Apellidos</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control"
                                                id="apellidos"
                                                name="apellidos"
                                                value={apellidos}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="tipodocumento" className="col-sm-2 col-form-label">Seleccione un tipo documento</label>
                                        <div className="col-sm-10">
                                            <Form.Select aria-label="Default select example"
                                                style={{ cursor: 'pointer' }}
                                                id="tipodocumento"
                                                name="tipodocumento"
                                                value={tipodocumento}
                                                onChange={onChange}
                                            >
                                                <option value="-8">SELECCIONE</option>
                                                <option value="1">CEDULA CIUDADANIA</option>
                                                <option value="2">CEDULA EXTRANJERIA</option>
                                                <option value="3">PASAPORTE</option>
                                                <option value="4">PEP</option>
                                                <option value="5">TARJETA DE IDENTIDAD</option>
                                            </Form.Select>
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="documento" className="col-sm-2 col-form-label">Documento</label>
                                        <div className="col-sm-10">
                                            <input type="number" className="form-control"
                                                id="documento"
                                                name="documento"
                                                value={documento}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="celular" className="col-sm-2 col-form-label">Celular</label>
                                        <div className="col-sm-10">
                                            <input type="number" className="form-control"
                                                id="celular"
                                                name="celular"
                                                value={celular}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="correo" className="col-sm-2 col-form-label">Email</label>
                                        <div className="col-sm-10">
                                            <input type="email" className="form-control"
                                                id="correo"
                                                name="correo"
                                                value={correo}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="direccion" className="col-sm-2 col-form-label">Dirección</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control"
                                                id="direccion"
                                                name="direccion"
                                                value={direccion}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="usuario_acceso" className="col-sm-2 col-form-label">Usuario Acceso</label>
                                        <div className="col-sm-10">
                                            <input type="email" className="form-control"
                                                id="usuario_acceso"
                                                name="usuario_acceso"
                                                value={usuario_acceso}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="clave_acceso" className="col-sm-2 col-form-label">Clave Acceso</label>
                                        <div className="col-sm-10">
                                            <input type="password" className="form-control"
                                                id="clave_acceso"
                                                name="clave_acceso"
                                                value={clave_acceso}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>


                                </div>

                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
}

export default UsuariosCrear;