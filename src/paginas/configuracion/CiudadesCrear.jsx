import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import APIInvoke from '../../helpers/APIInvoke.js';
import { useNavigate } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes';

const CiudadesCrear = () => {

    const navigate = useNavigate();

    const [ciudad, setCiudad] = useState({
        nombre: '',
        estado: 1
    });

    const { nombre } = ciudad;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, []);

    const onChange = (e) => {
        setCiudad({
            ...ciudad,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crear();
    }

    const crear = async () => {
        const body = {
            nombreCiudad: ciudad.nombre,
            estadoCiudad: ciudad.estado
        }
        const response = await APIInvoke.invokePOST(`/api/ciudades`, body);
        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            setCiudad({
                nombre: ''
            });
            navigate("/ciudades-admin");
        } else {
            mensajeConfirmacion('error', response.msg);
            setCiudad({
                nombre: '',
                estado: 1
            });
        }
    }

    return (
        <main id="main" className="main">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <ContentHeader
                Titulo={"Ciudades"}
                breadCrumb1={"Configuración"}
                breadCrumb2={"Listado Ciudades"}
                breadCrumb3={"Crear Ciudades"}
                ruta1={"/ciudades-admin"}
            />

            <section className="section">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <h5 className="card-title">Crear Ciudad</h5>

                                    <div className="row mb-3">
                                        <label htmlFor="nombre" className="col-sm-2 col-form-label">Nombre</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control"
                                                id="nombre"
                                                name="nombre"
                                                placeholder="Ingrese el nombre de la ciudad"
                                                value={nombre}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
}

export default CiudadesCrear;