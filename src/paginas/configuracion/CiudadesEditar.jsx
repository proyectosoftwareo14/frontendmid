import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import APIInvoke from '../../helpers/APIInvoke.js';
import { useNavigate, useParams } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes';
import Form from 'react-bootstrap/Form';

const CiudadesEditar = () => {

    //recibir parametros por la url
    const { id } = useParams();

    const navigate = useNavigate();

    const [ciudad, setCiudad] = useState({
        nombre: '',
        estado: ''
    });

    const { nombre, estado } = ciudad;

    const filaCiudadGuardada = async () => {
        const response = await APIInvoke.invokeGET(`/api/ciudades/${id}`);
        setCiudad({
            nombre: response.nombreCiudad,
            estado: response.estadoCiudad
        });
    }

    useEffect(() => {
        document.getElementById("nombre").focus();
        filaCiudadGuardada();
    }, []);

    const onChange = (e) => {
        setCiudad({
            ...ciudad,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editar();
    }

    const editar = async () => {
        const body = {
            nombreCiudad: ciudad.nombre,
            estadoCiudad: ciudad.estado
        }
        const response = await APIInvoke.invokePUT(`/api/ciudades/${id}`, body);
        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            setCiudad({
                nombre: ''
            });
            navigate("/ciudades-admin");
        } else {
            mensajeConfirmacion('error', response.msg);
            setCiudad({
                nombre: '',
                estado: ''
            });
        }
    }

    return (
        <main id="main" className="main">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <ContentHeader
                Titulo={"Ciudades"}
                breadCrumb1={"Configuración"}
                breadCrumb2={"Listado Ciudades"}
                breadCrumb3={"Editar Ciudades"}
                ruta1={"/ciudades-admin"}
            />

            <section className="section">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <h5 className="card-title">Editar Ciudad</h5>

                                    <div className="row mb-3">
                                        <label htmlFor="nombre" className="col-sm-2 col-form-label">Nombre</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control"
                                                id="nombre"
                                                name="nombre"
                                                placeholder="Ingrese el nombre del rol"
                                                value={nombre}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className="row mb-3">
                                        <label htmlFor="estado" className="col-sm-2 col-form-label">Seleccione el estado</label>
                                        <div className="col-sm-10">
                                            <Form.Select aria-label="Default select example"
                                                className="form-control"
                                                id="estado"
                                                name="estado"
                                                value={estado}
                                                onChange={onChange}
                                                style={{ cursor: 'pointer' }}
                                            >
                                                <option value="1">Activo</option>
                                                <option value="2">Inactivo</option>
                                            </Form.Select>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
}

export default CiudadesEditar;