import React, { useState, useEffect } from 'react';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import ContentHeader from '../../componentes/ContentHeader';
import APIInvoke from '../../helpers/APIInvoke.js';
import { Link } from 'react-router-dom';
import mensajeConfirmacion from '../../helpers/mensajes.js';

const CiudadesAdmin = () => {

    const [arreglo, setArreglo] = useState([]);

    const listadoDocumentos = async () => {
        const response = await APIInvoke.invokeGET(`/api/ciudades`);
        setArreglo(response);
    }

    useEffect(() => {
        listadoDocumentos();
    }, []);

    const borrar = async (e, id) => {
        e.preventDefault();

        const response = await APIInvoke.invokeDELETE(`/api/ciudades/${id}`);

        if (response.ok === "SI") {
            mensajeConfirmacion('success', response.msg);
            listadoDocumentos();
        } else {
            mensajeConfirmacion('error', response.msg);
        }
    }


    return (
        <main id="main" className="main">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <ContentHeader
                Titulo={"Ciudades"}
                breadCrumb1={"Configuración"}
                breadCrumb2={"Listado Ciudades"}
                breadCrumb3={""}
                ruta1={"/ciudades-admin"}
            />

            <section className="section">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Listado Ciudades</h5>
                                <div className='row mb-3'>
                                    <div className='col-lg-12'>
                                        <Link to={"/ciudades-crear"} className="btn btn-primary">Crear</Link>
                                    </div>
                                </div>
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style={{ width: '15%', textAlign: 'center' }}>Id</th>
                                            <th style={{ width: '65%', textAlign: 'center' }}>Ciudad</th>
                                            <th style={{ width: '10%', textAlign: 'center' }}>Estado</th>
                                            <th style={{ width: '10%', textAlign: 'center' }}>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            arreglo.map(
                                                elemento =>
                                                    <tr key={elemento._id}>
                                                        <td style={{ textAlign: 'center' }}>{elemento._id}</td>
                                                        <td>{elemento.nombreCiudad}</td>
                                                        <td style={{ textAlign: 'center' }}>
                                                            {elemento.estadoCiudad === 1 ? <span className="text-success">Activo</span> : <span className="text-danger">Inactivo</span>}
                                                        </td>
                                                        <td style={{ textAlign: 'center' }}>
                                                            <Link className="btn btn-primary btn-sm" to={`/ciudades-editar/${elemento._id}`}><i className="bi bi-pencil-square"></i></Link>
                                                            &nbsp;
                                                            <button className="btn btn-danger btn-sm" onClick={(e) => borrar(e, elemento._id)} style={{ cursor: 'pointer' }}>
                                                                <i className="bi bi-trash-fill"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
}

export default CiudadesAdmin;