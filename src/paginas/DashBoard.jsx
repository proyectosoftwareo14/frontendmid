import React from 'react';
import Navbar from '../componentes/Navbar';
import SidebarContainer from '../componentes/SidebarContainer';
import ContentHeader from '../componentes/ContentHeader';
import { Link } from 'react-router-dom';

const DashBoard = () => {
    return (
        <>
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>

            <main id="main" className="main">
                <ContentHeader
                    Titulo={"DashBoard"}
                    breadCrumb1={"DashBoard"}
                    breadCrumb2={""}
                    breadCrumb3={""}
                    ruta1={"/menu-principal"}
                />

                <section className="section dashboard">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="row">

                                <div class="col-xxl-3 col-md-6">
                                    <div class="card info-card sales-card">
                                        <div class="card-body">
                                            <h5 class="card-title"><span>Listado Roles</span></h5>
                                            <div class="d-flex align-items-center">
                                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i class="bi bi-person-fill"></i></div>
                                                <div class="ps-3">
                                                    <h6><Link to={"/roles-admin"}>Roles</Link></h6>
                                                    <span class="text-success small pt-1 fw-bold">...</span>
                                                    <span class="text-muted small pt-2 ps-1">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xxl-3 col-md-6">
                                    <div class="card info-card revenue-card">
                                        <div class="card-body">
                                            <h5 class="card-title"><span>Listado Ciudades</span></h5>
                                            <div class="d-flex align-items-center">
                                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i class="bi bi-map-fill"></i></div>
                                                <div class="ps-3">
                                                    <h6><Link to={"/ciudades-admin"}>Ciudades</Link></h6>
                                                    <span class="text-success small pt-1 fw-bold">...</span>
                                                    <span class="text-muted small pt-2 ps-1">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xxl-3 col-md-6">
                                    <div class="card info-card sales-card">
                                        <div class="card-body">
                                            <h5 class="card-title"><span>Listado Roles</span></h5>
                                            <div class="d-flex align-items-center">
                                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i class="bi bi-person-fill"></i></div>
                                                <div class="ps-3">
                                                    <h6><Link to={"/roles-admin"}>Roles</Link></h6>
                                                    <span class="text-success small pt-1 fw-bold">...</span>
                                                    <span class="text-muted small pt-2 ps-1">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xxl-3 col-md-6">
                                    <div class="card info-card sales-card">
                                        <div class="card-body">
                                            <h5 class="card-title"><span>Listado Roles</span></h5>
                                            <div class="d-flex align-items-center">
                                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i class="bi bi-person-fill"></i></div>
                                                <div class="ps-3">
                                                    <h6><Link to={"/roles-admin"}>Roles</Link></h6>
                                                    <span class="text-success small pt-1 fw-bold">...</span>
                                                    <span class="text-muted small pt-2 ps-1">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </main>
        </>
    );
}

export default DashBoard;