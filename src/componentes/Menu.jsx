import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => {
    return (
        <ul className="sidebar-nav" id="sidebar-nav">
            <li className="nav-item">
                <Link className="nav-link collapsed" to={"/menu-principal"}>
                    <i className="bi bi-grid" />
                    <span>Dashboard</span>
                </Link>
            </li>{/* End Dashboard Nav */}

            <li className="nav-item">
                <Link className="nav-link collapsed" data-bs-target="#config-nav" data-bs-toggle="collapse" to={"#"}>
                    <i className="bi bi-gear-fill" /><span>Configuración</span><i className="bi bi-chevron-down ms-auto" />
                </Link>
                <ul id="config-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <Link to={"/roles-admin"}>
                            <i className="bi bi-circle" /><span>Roles</span>
                        </Link>
                    </li>

                    <li>
                        <Link to={"/ciudades-admin"}>
                            <i className="bi bi-circle" /><span>Ciudades</span>
                        </Link>
                    </li>

                    <li>
                        <Link to={"/usuarios-admin"}>
                            <i className="bi bi-circle" /><span>Usuarios</span>
                        </Link>
                    </li>
                </ul>
            </li>

            <li className="nav-item">
                <Link className="nav-link collapsed" data-bs-target="#inventario-nav" data-bs-toggle="collapse" to={"#"}>
                    <i className="bi bi-basket-fill" /><span>Inventario</span><i className="bi bi-chevron-down ms-auto" />
                </Link>
                <ul id="inventario-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <Link to={"#"}>
                            <i className="bi bi-circle" /><span>Categorias</span>
                        </Link>
                    </li>

                    <li>
                        <Link to={"#"}>
                            <i className="bi bi-circle" /><span>Productos</span>
                        </Link>
                    </li>
                </ul>
            </li>

            <li className="nav-item">
                <Link className="nav-link collapsed" data-bs-target="#pedidos-nav" data-bs-toggle="collapse" to={"#"}>
                    <i className="bi bi-shop" /><span>Pedidos</span><i className="bi bi-chevron-down ms-auto" />
                </Link>
                <ul id="pedidos-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <Link to={"#"}>
                            <i className="bi bi-circle" /><span>Listado Pedidos</span>
                        </Link>
                    </li>
                </ul>
            </li>

        </ul>
    );
}

export default Menu;