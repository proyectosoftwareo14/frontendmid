import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => {
    return (
        <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li className="nav-item">
                    <Link to={"/menu-principal"} className="nav-link">
                        <i className="nav-icon fas fa-th" />
                        <p>
                            DashBoard
                        </p>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"#"} className="nav-link">
                        <i className="nav-icon fas fa-copy" />
                        <p>
                            Layout Options
                            <i className="fas fa-angle-left right" />
                        </p>
                    </Link>
                    <ul className="nav nav-treeview">
                        <li className="nav-item">
                            <Link to={"#"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Top Navigation</p>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"#"} className="nav-link">
                                <i className="far fa-circle nav-icon" />
                                <p>Top Navigation + Sidebar</p>
                            </Link>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    );
}

export default Menu;